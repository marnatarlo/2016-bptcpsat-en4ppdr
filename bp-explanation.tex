The path redundancy of the (potentially large) wireless mesh networks, that
provides backhaul for the LTE access network, puts emphasis on the backhaul
routing protocol ability to exploit this redundancy, since the routing
protocol determines the way data is transported to/from the User Equipments (UEs). We have
standard congestion-agnostic strategies such as Multiprotocol Label Switching
(MPLS, RFC 5921) and Optimized Link State Routing (OLSR, RFC 3626, which in
absence of node mobility and failures is equivalent to MPLS for the purpose of
the paper), but also congestion-aware strategies such as backpressure-based
ones~\cite{7422412}. The novelty in these strategies is that they take routing
decisions by dynamically mapping the trajectory followed by each data packet to
the most underutilized path, hence exploiting the network redundancy. However,
these decisions may potentially make the path followed by consecutive packets of
the same flow disjoint. This congestion-awareness, in theory, can help to avoid
heavy utilized paths, hence smoothing the traffic from/to the field operators.
For this reason, we choose to compare a standard, single and shortest path
protocol OLSR and a backpressure-based protocol, \emph{BP-MR}. In the first
subsection we detail \emph{BP-MR per-packet}~\cite{7396680} for sake of
completeness, whereas in the second subsection we describe its
new \emph{per-flow} variant proposed in this paper.

\subsubsection{BP-MR per-packet}
For readers interested in the history of backpressure-based routing protocol,
there is a comprehensive survey of backpressure state-of-the-art
in~\cite{7422412}. The root concept consists in a centralized policy which
routes traffic in a multi-hop network by minimizing the sum of the queue
\emph{backlog}s in the network among time slots. In the original proposal, there
is a separate queue for each flow that passes through the node. Basically, if we
define as \emph{backlog} the queue size at nodes, the main idea of backpressure
is to give priority to links and paths that have higher differential
\emph{backlog} between neighbouring nodes.

From this set of proposals we refer the reader to \emph{BP-MR}~\cite{7396680},
because of its proven scalability and performance improvements
transporting UDP traffic over wireless mesh backhauls. Specifically,
decentralized routing decisions are performed, in each node, following a
two-stage process. Firstly, \emph{BP-MR} classifies data packets in a
per-interface queue system according to their final destination. Secondly,
\emph{BP-MR} employs geographic and congestion information to compute the best
possible next-hop, on a \emph{per-packet} basis, from all possible forwarding
options in the multi-radio backhaul node. The per-interface queue system
presents lower complexity than the original per-flow queuing system, a better
delay performance compared to state-of-the-art backhauling routing protocols,
and its distributed routing decision features contribute to the scalability and
applicability capabilities of \emph{BP-MR}.

\emph{BP-MR} obtains information about surrounding network congestion
conditions (queue backlogs) through the periodical exchange of control packets
called \emph{HELLO}. In a dense SC wireless mesh backhaul, with many concurrent flows and
plentiful of available paths, it is likely to expect a high degree of
variability in these information, so these control packets have to be exchanged
quite often, for instance every 100 ms. In the worst case, every 100 ms the
queue information is changed in such a way that the packets passing through
the node can be redirected through another path. Hence, the \emph{per-packet}
evaluation performed by \emph{BP-MR} could lead to different routing choices
even for subsequent packets, despite the fact that they belong or not to the
same end-to-end flow. Therefore, with a high probability all these packets,
spread over many paths, will be received out-of-order by the end point,
creating a problem for the TCP receiver.

One possible answer to this problem is selecting only similar
(equal-hops/costs) paths in order to mitigate the different path delays.
Unfortunately, one of the main feature of backpressure-based algorithms is
their greedy approach: the nodes have only a local knowledge of the network,
limited to their immediate  surrounding neighbours (1-hop), which is
incompatible with the full knowledge required to compare different end-to-end
paths.

\subsubsection{BP-MR per-flow}

To overcome the packet reordering problem, without losing intrinsic
characteristic of \emph{BP-MR} and the capability of circumvent congested
paths, we apply the concept of \emph{per-flow} path selection strategy to
\emph{BP-MR} itself. Even if this strategy shares the name with the
per-flow queuing system presented in the original backpressure proposal, our
proposal does not apply to the queuing system (that continues to be per-interface,
as the original \emph{BP-MR}) but instead on the way the routing decisions are made.
Through identifying a flow as an origin to destination
packet stream of a transport layer connection between two end-hosts, each node
maintains per-active flow state information, or in other words it maps the
packets of a flow to its pre-assigned path, calculated the first time the node
sees the flow. Nevertheless, a new flow has the
flexibility to route dynamically to any of the available paths, and so is
able to circumvent congested routes, without actually causing packet
reordering at the destination. However, forwarding table size increases with
the flows number, but we believe that with modern equipments and an efficient
software implementation the saturation point of the network, in which there is
no difference between any routing strategy, is reached well before the
impossibility to store and manage the forwarding table. Note also that in terms
of scalability, the state kept by each \emph{BP-MR} node is smaller than the
state required by the typical backpressure \emph{per-flow} queuing system and the use
of wild-cards rules can also significantly reduce the total state and
forwarding table size kept by each backhaul node. Furthermore, \emph{BP-MR}
introduces much less control overhead than schemes that need to set up and
maintain end-to-end paths (e.g., \emph{OLSR}), since \emph{BP-MR} nodes do not need to
acquire a complete view of the network.
