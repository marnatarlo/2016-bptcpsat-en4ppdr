After a natural or man-made disaster, commercial terrestrial networks often
fail to provide the necessary support to public protection and disaster relief
(PPDR) professionals. In the luckiest case they cannot sustain the sudden surge
of resource demands due to congestion problems; but more often, the network 
simply get destroyed by the disaster.

The rising of LTE as the main wireless technology for broadband communication is
now supported by the presence of portable infrastructures that
provide flexible solutions for establishing coverage and increasing capacity
after a disaster~\cite{ltewhite}. This portability finally concludes the process of
investigation of the feasibility of LTE as Radio Access Network (RAN) during
emergencies~\cite{simic2012feasibility}. Mainly, the coverage is provided by a
dense deployment formed by Small Cells (SCs), because increasing frequency
re-use by reducing cell size has historically been the most simple and
effective way to increase raw capacity~\cite{webb2007wireless}.

Each mobile node that provides radio access (ENodeB) should be backhauled, in
order to reach the Evolved Packet Core (EPC). Backhauling these (dense) LTE
deployments is a challenging problem that can be addressed by incorporating
wireless transport nodes into ENodeBs. These wireless transport nodes will
feature one or multiple point-to-point interfaces, hence enabling the creation
of a redundant path of wireless mesh backhaul, aiming to provide the capillarity
required by the emergency operators. In this way, with the support of an
appropriate routing protocol, the LTE traffic will be properly carried
from/towards the EPC.

Recent developments in satellite technologies are bringing the availability of
non-terrestrial high performance channels, in order to backhaul the EPC with
high performance channels towards the Internet, allowing field operators to
access on-line resources~\cite{casoni2015integration,6962153} while limiting the
deployment costs~\cite{7347943}. Overall, this system design allows to provide,
after a disaster which destroys existing terrestrial infrastructure-based networks, an
alternative but complete network usable for running existing services (e.g.
Push-To-Talk, PTT) as well as innovative services (without the intention of
being exhaustive, we can bring as examples health monitoring systems,
on-line resource gathering, video streaming for field operations, ...).

In this paper we simulate an emergency deployment, which consists in an LTE
access network backhauled by a wireless mesh that can access the Internet
through a satellite channel. On this environment, we perform an evaluation of
throughput and latency experienced by TCP connections, as well as monitoring 
the scalability as a function of the number of flows. 
We examine the interaction between routing
protocols, used to carry traffic inside the mesh, and three common TCP
congestion controls: Cubic, Hybla, and Vegas. The routing strategies analysed
include Optimized Link State Routing (OLSR) and \emph{BP-MR} (an existing
backpressure-based routing protocol), proposing also a new variant to
\emph{BP-MR} that takes \emph{per-flow} decisions, instead of the original
\emph{per-packet} strategy.

The experiments have been performed with ns-3\footnote{https://www.nsnam.org} 
and show that a
combination of TCP Vegas (a delay-based congestion control) with \emph{BP-MR
per-flow} offers the best performance to PPDR professionals, in the
aforementioned emergency scenario.

The remainder of this paper is organized as follows.
Section~\ref{sec:background} contains the necessary background on \emph{BP-MR}
(both the original \emph{per-packet} variant and the proposed \emph{per-flow}).
Section~\ref{sec:scenario} describes the reference scenario and the methodology
used to present the simulation results in Section~\ref{sec:performance}.
Section~\ref{sec:related} covers related work, and finally,
Section~\ref{sec:conclusion} concludes the paper.

