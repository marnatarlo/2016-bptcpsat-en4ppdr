
The reported values of download time and RTT are represented in candlesticks,
where the boxes stretches from the 20th to the 80th percentiles and the whiskers
represent the maximum and minimum values, with the average value represented by
a black horizontal line.

\subsection{TCP response to different routing protocols}

\begin{figure*}
\centering
\captionsetup{justification=centering}
\includegraphics[width=.95\textwidth]{figures/sat-results/TCP/2UE-ss-1Mbps-2M-TCP-different.eps}
\caption{RTT and file download time using different TCP for downloading 2 MB, 2 concurrent UE.}
\label{fig:evaluation_different_tcp}
\end{figure*}

In Figure~\ref{fig:evaluation_different_tcp} we present the results obtained by
employing TCP Cubic, Hybla, and Vegas over different routing protocols during a
2 MB transfer from the remote node to 2 UEs (download), placed on the left side
of the mesh network. On the left, we can see the
reported RTT of the flows, measured during the transfer while, on the right, it
is reported the completion time required to conclude the download by the UEs.

The first thing that is important to note is that the RTT is dominated by the
propagation delay of the satellite, the baseline RTT is in fact roughly 750 ms.
Vegas manifests a very polite behaviour with respect to the RTT figure of merit
containing the entire candlesticks between 750 and 800 ms presenting, in
general, a very stable performance regardless the routing protocol adopted.
Also Cubic is particularly stable with respect to the RTT measurements; its
values range from 750 to 950 ms, with candlesticks slightly wider if compared
with Vegas. A little improvement here is reported when OLSR routing is adopted.
To complete the RTT analysis of Figure~\ref{fig:evaluation_different_tcp},
Hybla registered the worst values almost doubling the baseline RTT of 750 ms
achieving 1.4 seconds of RTT. In this case, as well as Vegas, the routing
protocol adopted during the experiment does not affect the final performance.

By moving from the RTT figure of merit to the download completion time, the
evaluation changes. In fact, here it is clearly visible as the choice of the
routing protocol starts to affect the results. Vegas, which was outperforming
the other TCP variants in terms of RTT, here suffers higher completion times if
compared with Cubic and Hybla ones and higher variance (considering the
wideness of the candlesticks). Hybla, which was the worst TCP algorithm in
terms of RTT, here registers the best performance, with an average completion
time of 7.5 seconds and 17.5 s when BP-MR and OLSR routing protocols are used
respectively. It is remarkable as the use of BP-MR routing strategy saves 10
seconds of download time with respect the OLSR protocol. Finally Cubic, which
was manifesting a quite good performance in terms of RTT, even with the file
completion time is maintaining measures close to the best ones. The download
time with the BP-MR routing strategy is slightly under 10 seconds, while the
download time with the OLSR routing strategy is slightly under 20 seconds. The
candlestick wideness of Cubic is the lowest of the experiment, resulting in an
algorithm very stable in terms of completion time.

This first evaluation of Figure~\ref{fig:evaluation_different_tcp} gives us a
preliminary idea about the impact of the routing strategy, that seems intervene
mostly on the download completion time, and the impact of the TCP algorithm,
that seems to mostly affect the RTT of the transmission.

\begin{figure*}
\centering
\captionsetup{justification=centering}
\includegraphics[width=.95\textwidth]{figures/sat-results/TCP/8UE-ss-1Mbps-2M-TCP-different.eps}
\caption{RTT and file download time using different TCP for downloading 2 MB, 8 concurrent UE.}
\label{fig:different_8}
\end{figure*}


\begin{figure*}
\centering
\captionsetup{justification=centering}
\includegraphics[width=.95\textwidth]{figures/sat-results/TCP/16UE-ss-1Mbps-2M-TCP-different.eps}
\caption{RTT and file download time using different TCP for downloading 2 MB, 16 concurrent UE.}
\label{fig:different_16}
\end{figure*}

In Figures~\ref{fig:different_8} and~\ref{fig:different_16} the above
experiment is reproduced increasing the connected UEs to 8 and 16,
respectively. By looking at these two figures, some results are confirmed while
some others start to change. First of all, considering the RTT figure of merit
of Figures~\ref{fig:different_8} and~\ref{fig:different_16}, the better
performance of Vegas, when compared with Cubic and Hybla, is confirmed. Hybla
manifests a very stable behaviour with basically the same RTT range between 750
and 1400 ms regardless the routing strategy adopted. Cubic seems the TCP
algorithm that suffers more the increase of the simultaneously active UEs. In
the next subsections a detailed analysis regard the scalability will be
provided to investigate more on this aspect.

The effect of increasing the UEs simultaneously active (and so the number of
flows) is affecting in particular the download completion time results. In
fact, Hybla moves from best to the  worst performance inverting also the impact
of the routing strategy. If in Figure~\ref{fig:evaluation_different_tcp} Hybla
was reporting the best results, in conjunction with BP-MR routing strategy,
regardless of the variant, in Figure~\ref{fig:different_16} the same
combination of routing strategy and TCP protocol is reporting the worst-case
values with completion times of almost 45 seconds in average. Another
remarkable change is registered by Vegas which moves from worst TCP protocol to
the best. In fact, in Figures~\ref{fig:different_8} and~\ref{fig:different_16}
it registers the lowest download time of 20 and 30 (average values)
respectively. Both of these best-case values have been reported by adopting the
BP-MR per-flow routing strategy. The Cubic algorithm remains very close to the
best TCP in all the simulations. It was close to the result obtained by Hybla
in Figure~\ref{fig:evaluation_different_tcp} and it is close to the results
obtained by Vegas in Figures~\ref{fig:different_8} and~\ref{fig:different_16},
manifesting also a very good robustness with respect to the routing strategy
adopted in Figure~\ref{fig:different_16}.

\subsection{Scalability of different TCP algorithms}

In this subsection we evaluate how the TCP variants scale as a function of the
active UEs during the simulation, analysing also the impact of the routing
strategy adopted. All the figures reported in this subsection are organized as
in the previous ones. On the left, we report the RTT of the flows, measured
during the transfer, while on the right it is reported the completion time
required to conclude the download by the UEs. The download size considered for
all the experiments is 2 MB.

\begin{figure*}
\centering
\captionsetup{justification=centering}
\includegraphics[width=.95\textwidth]{figures/sat-results/UE-increase/TcpCubic-2M-ss-1Mbps-UE-increase.eps}
\caption{RTT and file download time using TCP Cubic for downloading 2 MB, from 2 to 16 concurrent UE.}
\label{fig:cubic_ue}
\end{figure*}

In Figure~\ref{fig:cubic_ue} are reported the results obtained by the Cubic
algorithm. Considering the RTT, it is easy to notice how Cubic starts to suffer
in terms of scalability yet with 4 active UEs. In fact, the variance/range of
the candlesticks move to 750-1400 ms while the average slightly increase as a
function of the active UEs. The routing strategy adopted does not affect
clearly the RTT performance of Cubic. Considering the download completion time,
an easy thing to note is that OLSR does not provide good performance if
compared with BP-MR strategies. This difference in terms of completion time
performance between  OLSR and BP-MR seems to be mitigated by the congestion
level of the network, in fact the difference is reduced as function of the
active UEs with almost equal results with 16 UEs.

\begin{figure*}
\centering
\captionsetup{justification=centering}
\includegraphics[width=.95\textwidth]{figures/sat-results/UE-increase/TcpHybla-2M-ss-1Mbps-UE-increase.eps}
\caption{RTT and file download time using TCP Hybla for downloading 2 MB, from 2 to 16 concurrent UE.}
\label{fig:hybla_ue}
\end{figure*}


In Figure~\ref{fig:hybla_ue} we presents the results obtained with TCP Hybla in
place. Considering the RTT performance of Hybla, it has a very poor performance
with a range between 750 and 1450 ms that is almost constant both a as a
function of the number of active UEs and as a function of the routing strategy
adopted. Minimum and maximum values as well as average and wideness scale well.
The same is not true if we consider the download completion time, in fact
Hybla starts with very similar result to Cubic with 1 and 2 nodes, but the
growing trend of Hybla has a stronger impact than the Cubic one, i.e. Cubic
scales better. In particular, with 16 UEs, Hybla reaches values higher than 40
seconds for the average download completion time. Again, as with Cubic, it is
possible to note that OLSR routing strategy scales better than BP-MR strategies
even when coupled with Hybla.

\begin{figure*}
\centering
\captionsetup{justification=centering}
\includegraphics[width=.95\textwidth]{figures/sat-results/UE-increase/TcpVegas-2M-ss-1Mbps-UE-increase.eps}
\caption{RTT and file download time using TCP Vegas for downloading 2 MB, from 2 to 16 concurrent UE.}
\label{fig:vegas_ue}
\end{figure*}

The Vegas algorithm is probably the most interesting one for both the figures of merit. In
Figure~\ref{fig:vegas_ue} it is possible to see how Vegas scales well in terms of RTT with 
very stable and controlled candlesticks, a part for the once obtained with 16 UEs in which
the range starts to increase. For the download completion time it is interesting how the
Vegas algorithm maintains a baseline of more than 10 seconds of completion time,
slightly higher than the other TCP variants that have a baseline under 10 seconds. The variance
and the average of the completion times is always better for the BP-MR routing strategies 
instead of the OLSR one. In particular the BP-MR per-flow strategy is the best in all the
experiments, with a lower maximum and average values as well as more controlled variance.
The trend of the BP-MR per-flow strategy also scales better as a function of the network
congestion growing less while the number of UEs increases.
