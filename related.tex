The history of \emph{BP-MR} started with \emph{BP}~\cite{bp}, a self-organized
backpressure routing protocol, that is a decentralized flavor of the original
centralized backpressure algorithm. For dealing with sparse networks,
Backpressure for Sparse Deployments (\emph{BS})~\cite{bs} included additional
extensions to \emph{BP}. In particular, \emph{BS} added a penalty function
able to overcome dead ends in a scalable and decentralized way. However,
\emph{BS} was designed to tackle sparse topologies where nodes are equipped
with a single backhaul radio and presented high inefficiencies in
multi-radio deployments (i.e. with multiple backhaul interfaces). To mitigate
such inefficiencies, we proposed in~\cite{7396680} \emph{BP-MR}, used in this
paper and detailed for sake of completeness in Section~\ref{sec:background}.

TCP performance over the LTE RAN has been investigated in both simulated
environment and over real data. For simulated environment, many works (such
as~\cite{6005714,abed2011behavior}) simulate the access network with simple
point-to-point links, with different properties, and so without taking into
account the complex dynamics of the Radio Resource Control (RRC) state machine
and the TCP protocol, thus invalidating the obtained results. Nguyen et al.
in~\cite{tcp-lte-nguyen} investigates the performance of TCP over the full RAN
stack, concluding that increasing the load in a cell can significantly throttle
the bandwidth available to a UE, thus increasing the experienced delay,
especially when the eNodeB maintains a large per-UE queue. This can invalidate
the estimated RTO value, causing unnecessary TCP timeouts even when no packets
are lost. Also, they concluded that radio-link handover can cause significant
performance degradation. Similar conclusions are drawn
in~\cite{lte-tcp-in-depth-real-networks}, where the authors analyze a
large-scale real LTE data set to study the impact of protocol and application
behaviors on the LTE network performance. For instance, they concluded that
some  TCP behaviors (such as not updating the RTT estimation using the
duplicate ACKs) can cause severe performance issues; in addition, the bandwidth
utilization ratio is usually below 50\% for large flows. The work provides
valuable insights on the interaction between TCP and LTE. However, the details
of the backhaul network are out of the analysis (e.g. the routing algorithm).
We aim to fill this hole by adding the analysis of different backhaul routing
protocols over many TCP variants assuming a constrained wireless mesh backhaul.

For analysis of TCP traffic over backpressure routing we refer
to~\cite{RadunovicHorizon}, that identify the packet reordering at the receiver
as the main issue with backpressure routing, and then proposes a delayed
reordering algorithm at the destination for keeping packet reordering to a
minimum. Other proposals use the MAC layer to perform such
scheduling, for example~\cite{nawab2014fair}. While using the MAC layer ties the
proposal to a specific technology (in~\cite{nawab2014fair} is used an IEEE
802.11-based wireless mesh network), we believe that avoiding reordering is more
profitable than re-ordering packets in later stages. Under this light, we
proposed the \emph{BP-MR per-flow} variant.
Furthermore, talking of backpressure-based algorithms,
in~\cite{seferoglu2014tcp} it is shown that TCP experiences incompatibilities
with backpressure strategies that maintain per-flow queues, hence leading to
unfairness between flows. In contrast to this work, we analyze the performance
of \emph{BP-MR}, that maintains per-interface queues, and that does not require
changes at the TCP layer.

Moving to a satellite environment, the importance of routing protocols has been
analyzed in~\cite{tcp_rout_sat}. In this work, a constellation of MEO or LEO
satellite is considered, and performance evaluation of New Reno and SACK are
carried out over two different routing strategies, shortest-path and an
arbitrary multi-path protocol, that selects any minimum-hop path at a point in
time. The choice is made to approximate the behavior of temporarily congested
satellites; in our work, we effectively employ a real routing protocol to
eliminate such approximation, and we use the state-of-art TCP protocols for
satellite environments. In~\cite{Wang200785} the routing protocol is used to
mitigate the effect of the handoff in LEO constellations, in order to avoid the
blindly retransmission (due to the handoff). Another work that considers TCP and
routing protocol in a satellite environment is~\cite{fail_tcp}. The authors
propose an improvement to a diversity routing strategy (i.e. a sublayer between
TCP and the network that replicates each transmitted packet and sends the
multiple copies along parallel paths) to overcome the issue of diversity routing
over a congested network, with a satellite scenario.

%In our work, each routing node of a multi-hop path from the source to the
%destination cooperates in sending information to its neighbor nodes. Under
%such cooperative view, we can compare \emph{BP-MR} with other routing protocol
%proposals. For instance, one of the most practical environment to evaluate these
%proposals is a Wireless Sensor Networks (WSN) deployment.
%In~\cite{razzaque2014qos} is presented a Distributed Adaptive Cooperative
%Routing (DACR) protocol, that starts from a path created by Ad hoc On-Demand
%Distance Vector (AODV) and then refined, hop by hop, by introducing delay and
%energy constraints gathered through a reinforcement learning method. The main
%difference with our work is that we use point-to-point links to model the
%wireless network, instead of the point-to-multipoint typical of WSN. In
%point-to-multipoint scenario, the quality of a link between two neighbors plays
%a crucial role in the routing decision, while that quality in our environment is
%fixed, and overtaken in importance by the queuing delays (not analyzed
%in~\cite{razzaque2014qos}).
